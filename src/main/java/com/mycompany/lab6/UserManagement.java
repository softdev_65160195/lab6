/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab6;
import java.util.ArrayList;
/**
 *
 * @author boat5
 */
public class UserManagement {
    public static void main(String[] args){
        User admin = new User("admin","Administrator","pass1234",'M','A');
        User user1 = new User("user1","User 1","pass1234",'M','U');
        UserService US = new UserService();
        US.addUser(admin);
        US.addUser(user1);
        System.out.println(US.getUser(0));
        System.out.println(US.getUser(1));
        System.out.println(US.getUsers());
    }
}
