/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab6;

/**
 *
 * @author boat5
 */
public class User {
    private int id;
    private String name;
    private String login;
    private String password;
    private char gender;
    private char role;
    private static int lastId = 1;
    
    public User(String name, String login, String password, char gender, char role){
        this(User.lastId++, name, login, password, gender, role);
    }

    public User(int id, String name, String login, String password, char gender, char role) {
        this.id = id;
        this.name = name;
        this.login = login;
        this.password = password;
        this.gender = gender;
        this.role = role;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getGenderString(){
        if(gender == 'M'){
            return "Male";
        }else{
            return "Female";
        }
    }
    
    public char getGender() {
        return gender;
    }

    public String getRoleString(){
        if(role == 'A'){
            return "Admin";
        }else {
            return "User";
        }
    }
    
    public char getRole() {
        return role;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public void setRole(char role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", name=" + name + ", login=" + login + ", password=" + password + ", gender=" + gender + ", role=" + role + '}';
    }
    
}
